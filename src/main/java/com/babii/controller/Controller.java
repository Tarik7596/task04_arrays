package com.babii.controller;

import com.babii.model.TaskA;
import com.babii.model.TaskB;
import com.babii.model.TaskC;

public class Controller {
    private TaskA taskA = new TaskA();
    private TaskB taskB = new TaskB();
    private TaskC taskC = new TaskC();

    public void taskAIntersection() {
        taskA.arrayInitialization(TaskA.array1);
        taskA.arrayInitialization(TaskA.array2);
        taskA.calculateIntersection();
    }

    public void taskADifference() {
        taskA.arrayInitialization(TaskA.array1);
        taskA.arrayInitialization(TaskA.array2);
        taskA.calculateIntersection();
        taskA.calculateDifference();
    }

    public void taskBDuplication() {
        taskB.arrayInitialization(TaskB.array);
        taskB.deleteDuplicates();
    }

    public void taskCRow() {
        taskC.arrayInitialization(TaskC.array);
        taskC.deleteRow();
    }
}
