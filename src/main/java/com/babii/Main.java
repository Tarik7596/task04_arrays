package com.babii;

import com.babii.view.StartProgram;

public class Main {
    public static void main(String[] args) {
        StartProgram startProgram = new StartProgram();
        startProgram.start();
    }
}
