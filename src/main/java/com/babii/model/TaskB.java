package com.babii.model;

import java.util.Scanner;

public class TaskB {
    public static String[] array = new String[10];
    public static String[] arrayWithoutDuplicates = new String[10];
    private Scanner scanner = new Scanner(System.in);

    public void arrayInitialization(String[] array) {
        System.out.println("Enter your elements:");
        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.next();
        }
    }

    public void deleteDuplicates() {
        int counter = 0;
        int arrayPosition = 0;
      label: for (String currentElement1 : array) {
            for (String currentElement2 : arrayWithoutDuplicates) {
                if (currentElement1.equals(currentElement2)) {
                    counter++;
                }
                if (counter >= 2) {
                    counter = 0;
                    continue label;
                }
            }
            arrayWithoutDuplicates[arrayPosition] = currentElement1;
            arrayPosition++;
        }
    }
}
