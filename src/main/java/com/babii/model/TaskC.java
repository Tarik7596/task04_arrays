package com.babii.model;

import java.util.Scanner;

public class TaskC {
    public static String[] array = new String[10];
    public static String[] arrayWithoutRows = new String[10];
    private Scanner scanner = new Scanner(System.in);

    public void arrayInitialization(String[] array) {
        System.out.println("Enter your elements:");
        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.next();
        }
    }

    public void deleteRow() {
        int counter = 0;
        for (int i = 0; i < array.length - 1; i++) {
            if (!(array[i].equals(array[i + 1]))) {
                arrayWithoutRows[counter] = array[i];
                counter++;
            }
            if (i == array.length - 2) {
                arrayWithoutRows[counter] = array[i + 1];
            }
        }
    }
}
