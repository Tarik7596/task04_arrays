package com.babii.model;

import java.util.Scanner;

public class TaskA {
    public static String[] array1 = new String[5];
    public static String[] array2 = new String[5];
    public static String[] intersection = new String[5];
    public static String[] symmetricDifference = new String[10];
    private Scanner scanner = new Scanner(System.in);
    private int counter;

    public void arrayInitialization(String[] array) {
        System.out.println("Enter your elements:");
        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.next();
        }
    }

    public void calculateIntersection() {
        counter = 0;
        for (String currentElement1 : array1) {
            label:  for (String currentElement2 : array2) {
                if (currentElement1.equals(currentElement2)) {
                    for (String currentElement3 : intersection) {
                        if (currentElement1.equals(currentElement3)) {
                            continue label;
                        }
                    }
                    intersection[counter] = currentElement1;
                    counter++;
                }
            }
        }
    }

    public void calculateDifference() {
        counter = 0;
        label: for (String currentElement1 : array1) {
            for (String currentElement2 : intersection) {
                if (currentElement1.equals(currentElement2)) {
                    continue label;
                }
            }
            symmetricDifference[counter] = currentElement1;
            counter++;
        }
        label: for (String currentElement1 : array2) {
            for (String currentElement2 : intersection) {
                if (currentElement1.equals(currentElement2)) {
                    continue label;
                }
            }
            symmetricDifference[counter] = currentElement1;
            counter++;
        }
    }
}
