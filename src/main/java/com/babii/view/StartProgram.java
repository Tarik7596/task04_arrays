package com.babii.view;

import com.babii.controller.Controller;
import com.babii.model.TaskA;
import com.babii.model.TaskB;
import com.babii.model.TaskC;
import java.util.Arrays;
import java.util.Scanner;

public class StartProgram {
    private Controller controller = new Controller();
    private Scanner scanner = new Scanner(System.in);

    public void start() {
        String menuNum;
        System.out.println("Enter 1 to resolve task A");
        System.out.println("Enter 2 to resolve task B");
        System.out.println("Enter 3 to resolve task C");
        menuNum = scanner.next();
        if (menuNum.equals("1")) {
            System.out.println("Enter 1 to show arrays' intersection");
            System.out.println("Enter 2 to show arrays' symmetric difference");
            menuNum = scanner.next();
            if (menuNum.equals("1")) {
                controller.taskAIntersection();
                taskAIntersectionView();
            } else if (menuNum.equals("2")) {
                controller.taskADifference();
                taskADifferenceView();
            } else {
                System.out.println("You entered a wrong number");
            }
        } else if (menuNum.equals("2")) {
            controller.taskBDuplication();
            taskBDuplicationView();
        } else if (menuNum.equals("3")) {
            controller.taskCRow();
            taskCRowView();
        } else {
            System.out.println("You entered a wrong number");
        }

    }

    private void taskAIntersectionView() {
        System.out.println("First array:");
        System.out.println(Arrays.toString(TaskA.array1));
        System.out.println("Second array:");
        System.out.println(Arrays.toString(TaskA.array2));
        System.out.println("Elements exist in both arrays:");
        System.out.println(Arrays.toString(TaskA.intersection));
    }

    private void taskADifferenceView() {
        System.out.println("First array:");
        System.out.println(Arrays.toString(TaskA.array1));
        System.out.println("Second array:");
        System.out.println(Arrays.toString(TaskA.array2));
        System.out.println("Elements exist only in one array:");
        System.out.println(Arrays.toString(TaskA.symmetricDifference));
    }

    private void taskBDuplicationView() {
        System.out.println("Initial array:");
        System.out.println(Arrays.toString(TaskB.array));
        System.out.println("Array with numbers that repeat no more than two times:");
        System.out.println(Arrays.toString(TaskB.arrayWithoutDuplicates));
    }

    private void taskCRowView() {
        System.out.println("Initial array:");
        System.out.println(Arrays.toString(TaskC.array));
        System.out.println("Array with numbers that repeat no more than one time in a row:");
        System.out.println(Arrays.toString(TaskC.arrayWithoutRows));
    }
}
